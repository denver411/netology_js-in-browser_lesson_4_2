'use strict'

document.addEventListener('DOMContentLoaded', () => {

  const bgrdElements = [];

  const canvas = document.getElementById('wall');
  canvas.width = document.documentElement.clientWidth;
  canvas.height = document.documentElement.clientHeight;

  const ctx = canvas.getContext('2d');

  class Element {
    constructor(startX, startY) {
      this.startX = startX;
      this.startY = startY;
      this.size = (0.1 + Math.random() * 0.5);
      this.borderWidth = this.size * 5;
      this.ctx = canvas.getContext('2d');
    }
  }

  class Circle extends Element {

    constructor(startX, startY) {
      super(startX, startY);
      this.radius = this.size * 12;
    }

    createCircle(x = this.startX, y = this.startY) {
      this.ctx.beginPath();
      this.ctx.arc(x, y, this.radius, 0, 2 * Math.PI);
      this.ctx.strokeStyle = '#ffffff'
      this.ctx.lineWidth = this.borderWidth;
      this.ctx.stroke();
      this.ctx.closePath();
    }

  }

  class Cross extends Element {

    constructor(startX, startY) {
      super(startX, startY);
      this.side = this.size * 20
      this.speed = 0.2 - (Math.random() * 0.4);
      this.angle = Math.random() * 360;
    }

    createCross(x = this.startX, y = this.startY) {
      this.ctx.save();
      this.ctx.beginPath();
      this.ctx.translate(x, y);
      this.ctx.rotate(this.angle * Math.PI / 180);
      this.angle += this.speed;
      this.ctx.moveTo(0 , 0);
      this.ctx.lineTo(0,this.side);
      this.ctx.moveTo(-this.side / 2, this.side / 2);
      this.ctx.lineTo(this.side / 2, this.side / 2);
      this.ctx.strokeStyle = '#ffffff'
      this.ctx.lineWidth = this.borderWidth;
      this.ctx.stroke();
      this.ctx.restore();
      this.ctx.closePath();
    }
  }

  function nextPointFunc() {
    let option = Math.random();
    if (option > 0.5) {
      return function nextPoint(time = Date.now()) {
        return {
          x: this.startX + Math.sin((50 + this.startX + (time / 10)) / 100) * 3,
          y: this.startY + Math.sin((45 + this.startX + (time / 10)) / 100) * 4
        };
      }
    } else {
      return function nextPoint(time = Date.now()) {
        return {
          x: this.startX + Math.sin((this.startX + (time / 10)) / 100) * 5,
          y: this.startY + Math.sin((10 + this.startX + (time / 10)) / 100) * 2
        }
      }
    }
  }

  function createBgrd() {
    const quantity = 25 + Math.random() * 100;

    for (let i = 0; i < quantity; i++) {
      let circleX = Math.random() * canvas.width;
      let circleY = Math.random() * canvas.height;
      let crossX = Math.random() * canvas.width;
      let crossY = Math.random() * canvas.height;

      let newCircle = new Circle(circleX, circleY);
      newCircle.nextPoint = nextPointFunc();
      bgrdElements.push(newCircle);

      let newCross = new Cross(crossX, crossY);
      newCross.nextPoint = nextPointFunc();
      bgrdElements.push(newCross);

      newCircle.createCircle();
      newCross.createCross();
    }

  }

  setInterval(() => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    bgrdElements.forEach(item => {
      if (item instanceof Cross) {
        item.createCross(item.nextPoint(Date.now()).x, item.nextPoint(Date.now()).y);
      } else if (item instanceof Circle) {
        item.createCircle(item.nextPoint(Date.now()).x, item.nextPoint(Date.now()).y);
      }
    })
  }, 20 / 1000)

  createBgrd();

})