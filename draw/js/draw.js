'use strict';

const drawArea = document.getElementById("draw");
const ctx = drawArea.getContext("2d");

drawArea.width = document.documentElement.clientWidth;
drawArea.height = document.documentElement.clientHeight;

let drawing = false;
let lineWidth = 100;
let lineWidthStep = -1;
const lineColor = {
  hue: 0,
  saturation: '100%',
  lightness: '50%',
  hueStep: 1
}

window.addEventListener('resize', () => {
  ctx.clearRect(0, 0, drawArea.width, drawArea.height);
  drawArea.width = document.documentElement.clientWidth;
  drawArea.height = document.documentElement.clientHeight;
});

function drawCreate(x, y, shift = false) {
  ctx.beginPath();
  ctx.lineWidth = lineWidth;
  ctx.lineJoin = 'round';
  ctx.lineCap = 'round';
  ctx.strokeStyle = `hsl(${lineColor.hue}, ${lineColor.saturation}, ${lineColor.lightness})`;

  ctx.lineTo(x, y);

  ctx.stroke();

  lineWidth += lineWidthStep;
  if (lineWidth <= 5) {
    lineWidthStep = 1;
  } else if (lineWidth >= 100) {
    lineWidthStep = -1;
  }
  if (shift) {
    lineColor.hue -= lineColor.hueStep;
  } else {
    lineColor.hue += lineColor.hueStep;
  }
  if (lineColor.hue <= 0) {
    lineColor.hue = 359;
  } else if (lineColor.hue >= 359) {
    lineColor.hue = 0;
  }

}

drawArea.addEventListener("mousedown", (event) => {
  drawing = true;
  drawCreate(event.offsetX, event.offsetY);
});

drawArea.addEventListener("mouseup", () => {
  drawing = false;
});

drawArea.addEventListener("mouseleave", () => {
  drawing = false;
});

drawArea.addEventListener("mousemove", (event) => {
  if (drawing) {
    if (event.shiftKey) {
      drawCreate(event.offsetX, event.offsetY, true);
    } else {
      drawCreate(event.offsetX, event.offsetY);
    }
  }
});

drawArea.addEventListener('dblclick', () => {
  ctx.clearRect(0, 0, drawArea.width, drawArea.height);
});